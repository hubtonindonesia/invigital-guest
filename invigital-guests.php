<?php
/*
Plugin Name: Invigital Guests
Description: Guest Module for Invigital Theme
Author: Hubton
Version: 0.0.1
Author URI: https://hubton.com
Text Domain: invigital-guests
*/

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

require __DIR__.'/vendor/autoload.php';

define('IVG_GUESTS_FILE', __FILE__);
define('IVG_GUESTS_PATH', dirname( __FILE__ ));
define('IVG_GUESTS_DIR_URL', plugin_dir_url( __FILE__ ));

/**
 * Returns the core instance.
 *
 * @return IVG\Guests\Core
 */
function IVGGuests() {
	return IVG\Guests\Core::instance();
}

// Initialize Plugin
IVGGuests();
