(function ($) {
    // Clipboard
    var clipboard = new ClipboardJS('.ivg-button-copy');
    
    clipboard.on('success', function (e) {
        e.clearSelection();
        var $button = $(e.trigger)
        $button.addClass('copied');
        $button.one('mouseout', function () {
            $button.removeClass('copied');
        });
    });
})(jQuery);