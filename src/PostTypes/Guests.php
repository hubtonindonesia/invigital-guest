<?php

namespace IVG\Guests\PostTypes;

use IVG\Guests\Helpers as helpers;

class Guests
{
    protected $post_type = 'ivg_guest';

    /**
     * Class constructor
     */
    public function __construct()
    {
        // Register post type
        add_action( 'init', [ $this, 'register_post_type' ] );

        // Save post hook
        add_action( 'save_post_' . $this->post_type, [ $this, 'generate_guest_code' ], 20, 2 );

        // Add custom column
        add_filter( 'manage_'. $this->post_type .'_posts_columns', [ $this, 'custom_column_list' ] );
        add_action( 'manage_'. $this->post_type .'_posts_custom_column', [ $this, 'custom_columns' ], 10, 2 );
    }

    /**
     * Register the post type
     */
    public function register_post_type()
    {
        if (!function_exists('register_extended_post_type')) {
            return;
        }

        register_extended_post_type($this->post_type, [
            'has_archive'      => true,
            'hierarchical'     => false,
            'supports'         => ['title'],
            'dashboard_glance' => false,
            'enter_title_here' => __("Enter the Guest's name here", 'invigital-guests'),
            'menu_icon'        => 'dashicons-groups',
        ], [
            'singular' => __('Guest', 'invigital-guests'),
            'plural'   => __('Guests', 'invigital-guests'),
            'slug'     => 'invigital_guest',
        ]);
    }

    /**
     * Generate guest code on creation of guest
     * 
     * @param Integer $post_id
     * @param \WP_Post $post
     */
    public function generate_guest_code( $post_id, $post )
    {
        // If this is just a revision, don't create the code
        if ( wp_is_post_revision( $post_id ) )
            return;

        // Check for custom code
        if ( helpers\get_guest_code($post_id) == '' ) {
            $token = helpers\generate_guest_token(6);
            helpers\update_guest_code( $post_id, $token );
        }
    }

    /**
     * Custom Column
     */
    public function custom_column_list($columns)
    {
        $columns = [
            'cb'      => '<input type="checkbox" />',
            'title'   => __('Name', 'invigital-guests'),
            'code'    => __('Guest Code', 'invigital-guests'),
            'message' => __('Message', 'invigital-guests'),
        ];

        return $columns;
    }

    public function custom_columns( $column, $post_id )
    {
        if( 'code' == $column ) {
            echo '<strong>' . helpers\get_guest_code( $post_id ) . '</strong>';
        }

        if ( 'message' == $column ) {
            $message = helpers\get_invitation_message( $post_id );
            echo '<div class="ivg-guest-message" id="message-text'. $post_id .'">'. $message .'</div>';
            echo '<button type="button" class="button ivg-button-copy" data-clipboard-target="#message-text'. $post_id .'">';
                echo __('Copy', 'invigital-guests');
                echo '<span class="tooltip">'. __('Copied!', 'invigital-guests') .'</span>';
            echo '</button>';
        }
    }
}
