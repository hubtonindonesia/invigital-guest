<?php

namespace IVG\Guests;

class Admin
{
    public function __construct()
    {
        add_action( 'admin_enqueue_scripts', [ $this, 'enqueue_admin_assets' ]);
    }

    public function enqueue_admin_assets($hook)
    {
        if ('edit.php' != $hook) {
            return;
        }

        if ( $_GET['post_type'] != 'ivg_guest' ) {
            return;
        }

        wp_enqueue_style( 'ivg/guests/admin', IVG_GUESTS_DIR_URL . 'assets/css/admin.css');
        wp_enqueue_script( 'ivg/guests/clipboard', 'https://cdn.jsdelivr.net/npm/clipboard@2.0.6/dist/clipboard.min.js', [], '2.0.6', true );
        wp_enqueue_script( 'ivg/guests/admin', IVG_GUESTS_DIR_URL . 'assets/js/admin.js', ['jquery', 'ivg/guests/clipboard'], '0.0.1', true );
    }
}