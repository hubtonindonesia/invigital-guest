<?php

namespace IVG\Guests;

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

final class Core
{
    /**
     * The single instance of the class.
     *
     * @var Core
     */
    protected static $instance;

    /**
     * @var Options
     */
    var $options;

    /**
     * Class constructor.
     */
    protected function __construct()
    {
        add_action( 'plugins_loaded', [ $this, 'setup' ] );
    }

    /**
     * Ensures only one instance is loaded.
     *
     * @return Core
     */
    public static function instance() {
        if ( is_null( self::$instance ) ) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Setup Plugin
     */
    public function setup()
    {
        // Setup text domain
        $this->setup_textdomain();

        // Include helpers.
        require_once  IVG_GUESTS_PATH . '/src/helpers.php';

        // Setup post types
        $this->setup_post_types();

        // Setup page options
        $this->setup_page_options();

        new GuestImporter();

        new Admin();
    }

    /**
     * Setup Text Domain
     */
    public function setup_textdomain()
    {
        load_plugin_textdomain('invigital-guests', false, basename( IVG_GUESTS_PATH ) . '/languages/');
    }

    /**
     * Setup Post Types
     */
    public function setup_post_types()
    {
        $post_types_dir = IVG_GUESTS_PATH . '/src/PostTypes';

        foreach ( glob( $post_types_dir . '/*.php' ) as $filepath ) {
            $object_name = basename( $filepath, '.php' );
            $class_name = '\IVG\\Guests\\PostTypes\\' . $object_name;

            if ( class_exists($class_name) ) {
                $instance = new $class_name;
            }
        }
    }

    /**
     * Setup page options
     */
    public function setup_page_options()
    {
        $this->options = new Options();
    }
}
