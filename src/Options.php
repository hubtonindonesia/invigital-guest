<?php

namespace IVG\Guests;

class Options
{
    var $option_name;

    /**
     * Class constructor
     */
    public function __construct()
    {
        $this->option_name = 'ivg_guests_options';
        add_action( 'admin_init', [ $this, 'settings_init' ] );
        add_action( 'admin_menu', [ $this, 'options_page' ] );
    }

    /**
     * Initialize the settings
     */
    public function settings_init()
    {
        // Register a new setting
        register_setting('ivg_guests', $this->option_name);

        // Register a new section in the settings page
        add_settings_section(
            'ivg_section_message',
            __('Options', 'invigital-guests'),
            [ $this, 'section_callback' ],
            'ivg_guests'
        );

        add_settings_field(
            'ivg_guests_field_message',
            __('Invitation Message', 'invigital-guests'),
            [ $this,  'field_message_cb' ],
            'ivg_guests',
            'ivg_section_message',
            [
                'label_for' => 'invitation_message',
            ]
        );
    }

    /**
     * Section Callback
     */
    public function section_callback($args)
    {
    }

    /**
     * Render the message field
     */
    public function field_message_cb($args)
    {
        $options = get_option( $this->option_name );
        ?>
            <textarea class="large-text" rows="6" id="<?php echo esc_attr( $args['label_for'] );?>" name="<?php echo $this->option_name; ?>[<?php echo esc_attr( $args['label_for'] ); ?>]"><?php echo isset( $options[ $args['label_for'] ] ) ? $options[ $args['label_for'] ] : ''; ?></textarea>
            <p class="description">
                This message will be shown in the Guest list, so you can easily copy paste it and sent it to your guest. <br/>
                You can use below placeholder inside the message. <br/>
                <strong>{guest_name}</strong>: Show the Guest name<br/>
                <strong>{url}</strong>: URL to the website
            </p>
        <?php
    }

    /**
     * Show the options page
     */
    public function options_page()
    {
        add_menu_page(
            'Invigital',
            'Invigital', 
            'manage_options',
            'ivg_guests',
            [$this, 'options_page_html']
        );
    }

    public function options_page_html()
    {
        // check user capabilities
        if ( ! current_user_can( 'manage_options' ) ) {
            return;
        }
            
        // add error/update messages
            
        // check if the user have submitted the settings
        // wordpress will add the "settings-updated" $_GET parameter to the url
        if ( isset( $_GET['settings-updated'] ) ) {
            // add settings saved message with the class of "updated"
            add_settings_error( 'ivg_guests_message', 'ivg_guests_message', __( 'Settings Saved', 'invigital-guests' ), 'updated' );
        }
            
        // show error/update messages
        settings_errors( 'ivg_guests_message' );
        ?>
        
        <div class="wrap">
            <h1><?php echo esc_html( get_admin_page_title() ); ?></h1>

            <form action="options.php" method="post">
            <?php
                // output security fields for the registered setting
                settings_fields( 'ivg_guests' );
            
                // output setting sections and their fields
                do_settings_sections( 'ivg_guests' );
            
                // output save settings button
                submit_button( 'Save Settings' );
            ?>
            </form>
        </div>
        <?php
    }
}
