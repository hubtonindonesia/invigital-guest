<?php

namespace IVG\Guests;

use IVG\Guests\Helpers as helpers;

class GuestImporter
{
    var $importer_page;

    public function __construct()
    {
        add_action( 'admin_menu', [ $this, 'register_screen' ], 20 );

        add_action( 'admin_notices', [ $this, 'admin_notices' ], 10 );
    }

    /**
     * Register submenu page
     */
    public function register_screen()
    {
        $this->importer_page = add_submenu_page(
            'ivg_guests', 
            __('Guest Importer', 'invigital-guests'),
            __('Guest Importer', 'invigital-guests'), 
            'manage_options',
            'ivg_guest_importer',
            [ $this, 'importer_page' ]
        );

        add_action( 'load-' . $this->importer_page, array( $this, 'process_import' ) );
    }

    /**
     * Admin notices
     */
    public function admin_notices()
    {
        if ( ! isset( $_GET['page'] ) || ( $_GET['page'] != 'ivg_guest_importer' ) ) {
            return;
        }

        if ( isset( $_GET['error'] ) && $_GET['error'] == 'true' ) {
            echo '<div id="message" class="error"><p>' . __( 'There was a problem importing your settings. Please Try again.', 'invigital-guests' ) . '</p></div>';
        } else if ( isset( $_GET['invalid'] ) && $_GET['invalid'] == 'true' ) {  
            echo '<div id="message" class="error"><p>' . __( 'The import file you\'ve provided is invalid. Please try again.', 'invigital-guests' ) . '</p></div>';
        } else if ( isset( $_GET['imported'] ) && $_GET['imported'] == 'true' ) {  
            echo '<div id="message" class="updated"><p>' . sprintf( __( "Guest successfully imported. | View %sAll Guests%s", 'invigital-guests' ), '<a href="' . admin_url( 'edit.php?post_type=ivg_guest' ) . '">', '</a>' ) . '</p></div>';
        }
    }

    /**
     * Output Importer Page
     */
    public function importer_page()
    {
        // check user capabilities
        if ( ! current_user_can( 'manage_options' ) ) {
            return;
        }
        ?>

            <div class="wrap">
                <h1><?php echo esc_html( get_admin_page_title() ); ?></h1>

                <p><?php _e('Upload list of guest here in CSV format.', 'invigital-guests'); ?></p>
                <p><?php _e( sprintf('Download the sample CSV file <a href="%s" download>here</a>', IVG_GUESTS_DIR_URL . 'guest-sample.csv'), 'invigital-guests');?></p>

                <div class="form-wrap">
                    <form enctype="multipart/form-data" method="post" action="<?php echo admin_url( 'admin.php?page=ivg_guest_importer' ); ?>">
                        <?php wp_nonce_field( 'ivg-guest-import' ); ?>
                        <label for="ivg-import-file"><?php printf( __( 'Upload File: (Maximum Size: %s)', 'invigital-guests' ), ini_get( 'post_max_size' ) ); ?></label>
                        <input type="file" id="ivg-import-file" name="ivg-import-file" size="25" />
                        <input type="hidden" name="ivg-guest-import" value="1" />
                        <input type="submit" class="button" value="<?php _e( 'Upload File and Import', 'invigital-guests' ); ?>" />
                    </form>
                </div>
            </div>

        <?php
    }

    /**
     * Process Import
     */
    public function process_import()
    {
        if ( isset( $_POST['ivg-guest-import'] ) && ( $_POST['ivg-guest-import'] == true ) ) {
            check_admin_referer( 'ivg-guest-import' ); // Security check.

            if ( ! isset( $_FILES['ivg-import-file'] ) ) {
                return;
            }

            // If invalid file
            if( $_FILES['ivg-import-file']['type'] != 'text/csv' ) {
                wp_redirect( admin_url( 'admin.php?page=ivg_guest_importer&invalid=true' ) );
                exit;
            }

            $has_imported = false;

            // Extract file contents
            // Check to make sure its a successfull upload
            if ($_FILES['ivg-import-file']['error'] === UPLOAD_ERR_OK) {
                ini_set('auto_detect_line_endings',TRUE);
                $file = $_FILES['ivg-import-file']['tmp_name'];
                $csv_file = fopen($file, "r");
                $counter = 0;

                while (($data_file = fgetcsv($csv_file)) !== false) {
                    if ( $counter != 0 && count( $data_file ) > 0 ) {
                        // Insert guest data
                        $guest_id = wp_insert_post( array(
                            'post_title'  => $data_file[0],
                            'post_status' => 'publish',
                            'post_type'   => 'ivg_guest',
                        ) );

                        if( !is_wp_error( $guest_id ) ) {
                            $token = helpers\generate_guest_token(6);
                            helpers\update_guest_code( $guest_id, $token );
                            // update_field( 'field_555c2e09cbe28', $this->get_token( 6 ), $guest_id );
                            $has_imported = true;
                        }
                    }

                    $counter++;
                }

                ini_set('auto_detect_line_endings',FALSE);

                fclose( $file );

                // Redirect, add success flag to the URI
                if( $has_imported ) {
                    wp_redirect( admin_url( 'admin.php?page=ivg_guest_importer&imported=true' ) );
                    exit;
                }

                else {
                    wp_redirect( admin_url( 'admin.php?page=ivg_guest_importer&error=true' ) );
                    exit;	
                }
            }
        }
    }
}