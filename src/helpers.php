<?php

namespace IVG\Guests\Helpers;

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

/**
 * Get Guest code
 * 
 * @param Integer $guest_id
 * @return String
 */
function get_guest_code( $guest_id )
{
    return get_post_meta( $guest_id, 'guest_code', true );
}

/**
 * Update Guest code
 */
function update_guest_code( $guest_id, $token )
{
    return update_post_meta( $guest_id, 'guest_code', $token );
}

/**
 * Check Guest Code
 */
function get_guest_id_from_code( $code )
{
    global $wpdb;

    $query = $wpdb->prepare( "SELECT post_id FROM $wpdb->postmeta WHERE meta_key = 'guest_code' AND meta_value = %s", $code );
    $results = $wpdb->get_var( $query );

    if( $results ) {
        return $results;
    } else {
        return false;
    }
}

/**
 * Generate guest token
 * 
 * @param Integer $length Token Length
 * @return String
 */
function generate_guest_token( $length )
{
    $token = "";
    $alphaNumeric = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    $alphaNumeric.= "0123456789";
    for( $i=0; $i < $length; $i++){
        $token .= $alphaNumeric[ crypto_rand_secure(0, strlen($alphaNumeric) ) ];
    }
    return $token;
}

/**
 * Generate random string
 * 
 * @param Integer $min
 * @param Integer $max
 * @return Integer
 */
function crypto_rand_secure($min, $max)
{
    $range = $max - $min;
    if ($range < 0) return $min; // not so random...

    $log    = log($range, 2);
    $bytes  = (int) ($log / 8) + 1;    // length in bytes
    $bits   = (int) $log + 1;          // length in bits
    $filter = (int) (1 << $bits) - 1;  // set all lower bits to 1
    do {
        $rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
        $rnd = $rnd & $filter; // discard irrelevant bits
    } while ($rnd >= $range);

    return $min + $rnd;
}

/**
 * Get guest url with guest code
 * 
 * @param Integer $guest_id
 */
function get_guest_url( $guest_id )
{
    $code = get_guest_code( $guest_id );
    
    return add_query_arg('code', $code, home_url('/'));
}

/**
 * Get the invitation message
 * 
 * @param Integer $guest_id
 */
function get_invitation_message( $guest_id )
{
    $options = get_option(IVGGuests()->options->option_name);
    
    if ($options['invitation_message']) {

        $search = [
            '{guest_name}',
            '{url}',
        ];

        $replace = [
            get_the_title($guest_id),
            get_guest_url($guest_id),
        ];

        $message = str_replace($search, $replace, $options['invitation_message']);

        return $message;
    }

    return;
}